class StaticPagesController < ApplicationController

  def home
    if logged_in?
      @micropost  = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
    end
  end

  def help
  end
  
  def statspage
  end

def about

 @name = Lawnbowl.all
  

end

def errory
   @name = Lawnbowl.all
  

end
  

  def contact
  end
  
  def newgames
  end 
  
  def game
  end
  

  
  def shelfhallpark

    shelf = Lawnbowl.find_by(name: ' Shelf Hall Park ')
    @name = shelf.name
    @address = shelf.address
    @owner = shelf.owner

  end
  
def saddleworthroad

    saddle = Lawnbowl.find_by(name: ' Saddleworth Road ')
    @name = saddle.name
    @address = saddle.address
    @owner = saddle.owner

 end

def akroydonvictoriabowlingclub

    akroy = Lawnbowl.find_by(name: ' Akroydon Victoria Bowling Club ')
    @name = akroy.name
    @address = akroy.address
    @owner = akroy.owner

 end
 
 def akroydpark

    park = Lawnbowl.find_by(name: ' Akroyd Park Bowling Greens ')
    @name = park.name
    @address = park.address
    @owner = park.owner

 end
 
 def alanpark
   
   alan = Lawnbowl.find_by(name: ' Alan Park ')
   @name = alan.name
   @address = alan.address
   @owner = alan.owner
   
 end
 
 
def albionbowlingclub
  
  albion = Lawnbowl.find_by(name: ' Albion Bowling Club ')
  @name = albion.name
  @address = albion.address
  @owner = albion.owner
  
end

def bailiffbridgeclub
  
  baili = Lawnbowl.find_by(name: ' Bailiff Bridge Club ')
  @name = baili.name
  @address = baili.address
  @owner = baili.owner
end

def bradshawparl
  
  brad = Lawnbowl.find_by(name: ' Bradshaw Park ')
  @name = brad.name
  @address = brad.address
  @owner = brad.owner
end

def brighousesportsclub
  
  brigde = Lawnbowl.find_by(name: ' Brighouse Sports Club ')
  @name = brigde.name
  @address = brigde.address
  @owner = brigde.owner
end


def centrevalepark
  
  center = Lawnbowl.find_by(name: ' Centre Vale Park ')
  @name = center.name
  @address = center.address
  @owner = center.owner
end

def crowwoodpark
  
  crow = Lawnbowl.find_by(name: ' Crow Wood Park ')
  @name = crow.name
  @address = crow.address
  @owner = crow.owner
end

def crossleyssportground
  
  cross = Lawnbowl.find_by(name: ' Crossleys Sports Ground ')
  @name = cross.name
  @address = cross.address
  @owner = cross.owner
  
    
    
end

def elland
  
  e = Lawnbowl.find_by(name: ' Elland W M C ')
  @name = e.name
  @address = e.address
  @owner = e.owner
end


def ellandcricketathetic
  
  ell = Lawnbowl.find_by(name: ' Elland Cricket & Athletic Bowling Club ')
  @name = ell.name
  @address = ell.address
  @owner = ell.owner
end

def greenroyd
  
  greeny = Lawnbowl.find_by(name: ' Greenroyd Bowling Club ')
 @name = greeny.name
 @address = greeny.address
 @owner = greeny.owner
end

def halifax
  
  halifaxy = Lawnbowl.find_by(name: ' Halifax Bowling Club ')
  @name = halifaxy.name
  @address = halifaxy.address
  @owner = halifaxy.owner
end

def heptonstall
  
  hep = Lawnbowl.find_by(name: ' Heptonstall Social & Bowling Club ')
  @name = hep.name
  @address = hep.address
  @owner = hep.owner
end

def hillcrest
  
  hill = Lawnbowl.find_by(name: ' Hillcrest Bowling Club ')
  @name = hill.name
  @address = hill.address
  @owner = hill.owner
end

def hipperholme
  
  hipper = Lawnbowl.find_by(name: ' Hipperholme & Lightcliffe Bowling Club ')
  @name = hipper.name
  @address = hipper.address
  @owner = hipper.owner
end

def hollins
  
  holl = Lawnbowl.find_by(name: ' Hollins Road ')
  @name = holl.name
  @address = holl.address
  @owner = holl.owner
end

def hove 
  
  hov = Lawnbowl.find_by(name: ' Hove Edge Bowling & Working Mens Club ')
  @name = hov.name
  @address = hov.address
  @owner = hov.owner
end

def homefield
  
  home = Lawnbowl.find_by(name: ' Holmfield Park ')
  @name = home.name
  @address = home.address
  @owner = home.owner
end

def kingstonsocialclub
  
  king = Lawnbowl.find_by(name: ' Kingston Social Club ')
  @name = king.name
  @address = king.address
  @owner = king.owner
end


def green
  
  gree = Lawnbowl.find_by(name: ' Greenroyd Bowling Club ')
  @name = gree.name
  @address = gree.address
  @owner = gree.owner
end

def leemillroad
  
  lee = Lawnbowl.find_by(name: ' Lee Mill Road ')
  @name = lee.name
  @address = lee.address
  @owner = lee.owner
end

def mixenden
  
  mixy = Lawnbowl.find_by(name: ' Mixenden Park ')
  @name = mixy.name
  @address = mixy.address
  @owner = mixy.owner
end


def northowram
  
  north = Lawnbowl.find_by(name: ' Northowram Bowling Club ')
  @name = north.name
  @address = north.address
  @owner = north.owner
end

def outlane
  
  lane = Lawnbowl.find_by(name: ' Outlane Bowling Club ')
  @name = lane.name
  @address = lane.address
  @owner = lane.owner
end

def pellon
  
  pell = Lawnbowl.find_by(name: ' Pellon Lane Social Club ')
  @name = pell.name
  @address = pell.address
  @owner = pell.owner
end

def rastrick
  
  ras = Lawnbowl.find_by(name: ' Rastrick Bowling Club ')
  @name = ras.name
  @address = ras.address
  @owner = ras.owner
end

def ripponden
  
  ripp = Lawnbowl.find_by(name: ' Ripponden Bowling Club ')
  @name = ripp.name
  @address = ripp.address
  @owner = ripp.owner
end

def stainland
  
  stain = Lawnbowl.find_by(name:  ' Stainland Memorial Recreation Ground ')
  @name = stain.name
  @address = stain.address
  @owner = stain.owner
end

def shroggs
  
  shor = Lawnbowl.find_by(name: ' Shroggs Parks ')
  @name = shor.name
  @address = shor.address
  @owner = shor.owner
end

def siddal
  
  sid = Lawnbowl.find_by(name: ' Siddal Recreation Ground ')
  @name = sid.name
  @address = sid.address
  @owner = sid.owner
end


def sowerby
  
  sow = Lawnbowl.find_by(name: ' Sowerby District Bowling Club ')
  @name = sow.name
  @address = sow.address
  @owner = sow.owner
end

def spring
  
  sprin = Lawnbowl.find_by(name: ' Spring Hall Sports Ground ')
 @name = sprin.name
 @address = sprin.address
  @owner = sprin.owner
end

def wadworth
  
  wad = Lawnbowl.find_by(name: ' Wadsworth Community Centre ')
  @name = wad.name
  @address = wad.address
  @owner = wad.owner
end

def walsden
  
  wal = Lawnbowl.find_by(name: ' Walsden Cricket & Bowling Club ')
  @name = wal.name
  @address = wal.address
  @owner = wal.owner
end

def wellseley

  wel = Lawnbowl.find_by(name: ' Wellesley Park Bowling Green ')
  @name = wel.name
  @address = wel.address
  @owner = wel.owner
end

def west
  
  wee = Lawnbowl.find_by(name: ' West End Bowling Club ')
  @name = wee.name
  @address = wee.address
  @owner = wee.owner
end


def wharf 
  
  wha = Lawnbowl.find_by(name: ' Wharf Street Bowling Green ')
  @name = wha.name
  @address = wha.address
  @owner = wha.owner
end

def woodvale
  
  wood = Lawnbowl.find_by(name: ' Woodvale Bowling Club ')
  @name = wood.name
  @address = wood.address
  @owner = wood.owner
end

  
  
  def gallery
    
       @name = Lawnbowl.all
    
  end  

  

def results
  
hit = Lawnbowl.where("name LIKE ?", "%#{params[:search]}%").first
    if hit != nil
      @name = hit.name
      @address = hit.address
      @owner = hit.owner
    else
      redirect_to errory_path

  
  end 
end
end

