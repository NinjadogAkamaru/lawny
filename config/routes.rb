Rails.application.routes.draw do
  get 'scots/new'

  get 'lawnbowls/new'

  get 'password_resets/new'

  get 'password_resets/edit'

  get 'sessions/new'

  get 'users/new'

  root             'static_pages#home'
  get 'team'    => 'scots#team'
  get 'name'    =>  'lawnbowl#name'
  get 'gallery' => 'static_pages#gallery'
  get 'about'   => 'static_pages#about'
  get 'contact' => 'static_pages#contact'
  get 'shelfhallpark'     => 'static_pages#shelfhallpark'
  get 'saddleworthroad'   => 'static_pages#saddleworthroad'
  
  
  get 'akroydonvictoriabowlingclub'   => 'static_pages#akroydonvictoriabowlingclub'
  get 'akroydpark'   => 'static_pages#akroydpark'
  get 'alanpark'    => 'static_pages#alanpark'
  get 'albionbowlingclub'   => 'static_pages#albionbowlingclub'
  get 'bailiffbridgeclub'   => 'static_pages#bailiffbridgeclub'
  get 'bradshawparl'    => 'static_pages#bradshawparl'
  get 'brighousesportsclub'   => 'static_pages#brighousesportsclub'
  get 'calderholmespark'    => 'static_pages#calderholmespark'
  get 'centrevalepark'   =>  'static_pages#centrevalepark'
  get 'crossleyssportground'    => 'static_pages#crossleyssportground'
  get 'crowwoodpark'    => 'static_pages#crowwoodpark'
  get 'ellandcricketathetic'    => 'static_pages#ellandcricketathetic'
  get 'elland'    => 'static_pages#elland'
  get 'greenroyd'   => 'static_pages#greenroyd'
  get 'halifax'   => 'static_pages#halifax'
  get 'heptonstall'   => 'static_pages#heptonstall'
  get 'hillcrest'   => 'static_pages#hillcrest'
  get 'hipperholme'   => 'static_pages#hipperholme'
  get 'hollins'   => 'static_pages#hollins'
  get 'hove'    => 'static_pages#hove'
  get 'homefield'   => 'static_pages#homefield'
  get 'kingstonsocialclub'    => 'static_pages#kingstonsocialclub'
  get 'green'   => 'static_pages#green'
  get 'leemillroad'  => 'static_pages#leemillroad'
  get 'mixenden'    => 'static_pages#mixenden'
  get 'norland'  => 'static_pages#norland'
  get 'northowram'    => 'static_pages#northowram'
  get 'outlane'   => 'static_pages#outlane'
  get 'pellon'    => 'static_pages#pellon'
  get 'rastrick'    => 'static_pages#rastrick'
  get 'ripponden'   => 'static_pages#ripponden'
  get 'stainland'  => 'static_pages#stainland'
  get 'shroggs'    => 'static_pages#shroggs'
  get 'siddal'    => 'static_pages#siddal'
  get 'sowerby'   => 'static_pages#sowerby'
  get 'spring'    => 'static_pages#spring'
  get 'wadworth'    => 'static_pages#wadworth'
  get 'walsden'   => 'static_pages#walsden'
  get 'wellseley'   => 'static_pages#wellseley'
  get 'west'    => 'static_pages#west'
  get 'wharf'   => 'static_pages#wharf'
  get 'woodvale'    => 'static_pages#woodvale'
  get 'errory'   => 'static_pages#errory'
  get 'results'  => 'static_pages#results'
  get 'signup'  => 'users#new'
  get 'login'   => 'sessions#new'
  post 'login'  => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'
   resources :users do
    member do
      get :following, :followers
    end
  end
  resources :account_activations, only: [:edit]
  resources :password_resets,     only: [:new, :create, :edit, :update]
  resources :microposts,          only: [:create, :destroy]
  resources :relationships,       only: [:create, :destroy]
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
